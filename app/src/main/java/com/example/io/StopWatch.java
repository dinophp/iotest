package com.example.io;

import java.time.Clock;


public class StopWatch {
    private static long begin = 0l;
    private static long end = 0l;

    static void start() {
        begin = System.currentTimeMillis();
    }

    static long stop() {
        end = System.currentTimeMillis();
        return end - begin;
    }
}