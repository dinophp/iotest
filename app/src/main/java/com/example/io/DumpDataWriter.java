package com.example.io;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;


public class DumpDataWriter {
    static long size1MB = 1024 * 1024;
    static long size10MB = 1024 * 1024 * 10;
    static long size100MB = 1024 * 1024 * 100;
    static long size1000MB = 1024 * 1024 * 1000;
    static byte oneByte = 1;
    static String input1MB = "input1MB.txt";
    static String input10MB = "input10MB.txt";
    static String input100MB = "input100MB.txt";
    static String input1000MB = "input1000MB.txt";

    static byte[] byte2048 = new byte[2048];
    static byte[] byte4096 = new byte[4096];
    static byte[] byte8192 = new byte[8192];

    static {
        Arrays.fill(byte2048, (byte) 'a');
        Arrays.fill(byte4096, (byte) 'a');
        Arrays.fill(byte8192, (byte) 'a');

    }


    public static void main(String[] args) throws IOException {
        //writeDumpData(input1MB, size1MB);
        //writeDumpData(input10MB, size10MB);
        //writeDumpData(input100MB, size100MB);
        //writeDumpData(input1000MB, size1000MB);
    }

    static void writeDumpData(String filePath, int size) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(new File(filePath));

        int s = size / 8192;
        for (int i = 0; i < s; i++) {
            outputStream.write(byte8192);
        }
//        outputStream.flush();
        outputStream.close();


    }

    static void readData(String filePath) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(new File(filePath));
        byte[] b = new byte[8192];// 读取到的数据要写入的数组。
        while (fileInputStream.read(b) != -1) {

        }
    }

    static StringBuilder readDataNIO(String f) throws IOException {
        FileInputStream fin = new FileInputStream(new File(f));
        FileChannel fileChannel = fin.getChannel();
        ByteBuffer byteBuffer = ByteBuffer.allocate(8192);
        StringBuilder sb = new StringBuilder();

        while (fileChannel.read(byteBuffer) > 0) {
            byteBuffer.rewind();
            sb.append(new String(byteBuffer.array()));
            byteBuffer.flip();
        }
        fileChannel.close();
        fin.close();
        return sb;
    }
}